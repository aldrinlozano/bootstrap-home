jQuery(document).ready( function(){
	var now = new Date();
	var i = 0;
	var defaultContent = $('#welcome-text').html();
	var trends = "<h2>Manchester United<br>#Mourinho<br>Cristiano Ronaldo<br>#Napoles<br>#busban<br>Believer Tour in Pluto"+
					"<br>#RamseyScoredPrayHardBeliebersLOL<br>GGMU<br>#FACommunityShield</h2>";
	var discover = "<img src='discover.png'></img>";

	var welcome = $("#welcome-text");
	var imageCounter=0;

	function progressBar(){
		$("#progress-bar").css({"width": (i++)+"%"});
		if(i<55){
			$("#progress-bar").addClass("bar-danger");
		}
		if(i>55&&i<110)
			$("#progress-bar").removeClass("bar-danger");
		if(i==110){
			$("#actual-bar").fadeOut();
			//$("#progress-bar").remove();
		}
		if(i>=115&&i<130){	
			$("#notif").fadeIn();
		}
		else
			$("#notif").fadeOut();
	}
		
	setInterval(progressBar, 4000);

	function progressBar(){
		if(imageCounter==2)	
			imageCounter = -1;
		imageCounter++;
		$("#tweetcloud-image").fadeOut('slow', function() {
    		$("#tweetcloud-image").attr("src", "css/tweetcloud-sample"+imageCounter+".png");
    		$("#tweetcloud-image").fadeIn('slow');
		});
	}
	
	$(".nav-choice").on("click", function(){
		var tab = $(this).attr("id");
		if(tab=="trends")	tab = trends;
		else if(tab=="discover") tab = discover;
		welcome.fadeOut('fast', function() {
    		welcome.html(tab);
    		welcome.css("right","150px");
    		welcome.fadeIn('fast');
		});
	});

	$("#brand-name").on("click", function(){
		welcome.fadeOut('fast', function() {
    	welcome.removeAttr("style").html(defaultContent);
   		welcome.fadeIn('fast');
		});
	});

	$(".twitter-signin").on("click", function(){
		bootbox.alert("<span id='error'>Sorry but we're having problems with our servers at the moment.<br>Please try again later.</span>");
	});

});